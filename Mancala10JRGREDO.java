
//James Gillman
//10/25/2017
//Mancala P10
//Plays a game of Mancala! Draws a board,  Checks and outputs, what player is playing, whether or not their turn has ended, checks who wins and even handles ties! You can choose to go against BeepBoop the AI if you wish!
import java.util.Scanner;

public class Mancala10JRGREDO
{
	public static Scanner input = new Scanner ( System.in );
	final static int MAX = 14;

	public static void main( String[ ] args )
	{
		int[ ] beadArray = new int[MAX];
		initializeArray ( beadArray );
		int gameOver = -1; // Makes it so game can be over!
		int player = 1; // Which player is playing
		char robot = 'n'; // Is BeepBoop playing
		char hardMode = 'y'; // enables harder difficulty

		System.out.println ( "Hey fleshbag want to go against me, BeepBoop? (y/n)" );
		robot = input.next ( ).charAt ( 0 );
		if ( robot == 'y' )
		{
			System.out.println ( "Want me to be extra butt-ish to you? (y/n)" );
			hardMode = input.next ( ).charAt ( 0 );
			System.out.println ( "Bring it foolish human!" );
		}
		drawBoard ( beadArray );
		while ( gameOver == -1 )
		{
			dropBeads ( beadArray, player, robot, hardMode );
			gameOver = gameOverCheck ( beadArray );
			if ( player == 1 )
			{
				player = 2;
			}
			else
			{
				player = 1;
			}
		}

		if ( gameOver == 0 )
		{
			System.out.println ( " It's a Tie!!" );
		}
		else if ( gameOver == 1 )
		{
			System.out.println ( " Winner is Player 1!" );
		}
		else if ( robot == 'n' )
		{
			System.out.println ( "Winner is player 2!!!" );
		}
		else
		{
			System.out.println ( "Silly human, you cannot beat I, BeepBoop!" );
		}

	}// end of Main method!

	/*****************************
	 * MethodName: initializeArray Return: Void Parameters: beadArray the number of beads for each player
	 * Description:Puts the starting value for each position in the array.
	 */
	public static void initializeArray( int[ ] beadArray )
	{
		for ( int index = 0; index < MAX; index++ )
		{
			beadArray[index] = 4;
		}
		beadArray[6] = 0;
		beadArray[13] = 0;
	}

	/***********************************
	 * methodName: printSolidLine return: void parameters: starNum: however many stars we need to produce in a line.
	 * description: prints a solid line of stars.
	 */
	public static void printSolidLine( int starNum )
	{
		for ( int count = 0; count < starNum; count++ )
		{
			System.out.print ( "*" );
		}
	}// end of printSolidLine!

	/*************************************
	 * methodName: printDottedLine() 
	 * return: void 
	 * parameters: none 
	 * description: prints dotted line with 6 spaces in between.
	 */
	public static void printDottedLine( )
	{
		int lcv = 0;
		int count = 0;
		int lcv2 = 0;
		while ( 8 > lcv ) // makes sure 8 is bigger than loop control value
		{
			System.out.print ( "*" );
			count = 6;
			lcv2 = 0;
			while ( count > lcv2 ) // makes sure count is bigger than loop control value 2.
			{
				System.out.print ( " " );
				lcv2++;
			}

			lcv++;
		}
		System.out.print ( "*" );
		System.out.print ( "\n" );
	} // end of printDottedLine

	/***************************************
	 * MethodName: drawBoard 
	 * parameters: beadArray: number of beads for each player. 
	 * return: void 
	 * description: Creates the Mancala board and inputs Bin Numbers.
	 */
	public static void drawBoard( int[ ] beadArray )
	{
		printSolidLine ( 57 );
		System.out.print ( "\n" );
		printDottedLine ( );
		System.out.print ( "*      *" );
		showTopRowNumbers ( );
		printDottedLine ( );
		showTopArrayNumbers ( beadArray );
		printDottedLine ( );
		System.out.print ( "*  13  " );
		printSolidLine ( 43 );
		System.out.print ( "   6  *" );
		System.out.print ( "\n" );
		printDottedLine ( );
		System.out.print ( "*      *" );
		showBottomRowNumbers ( );
		printDottedLine ( );
		showBottomArrayNumbers ( beadArray );
		printDottedLine ( );
		printSolidLine ( 57 );
		System.out.print ( "\n" );

	}// end of drawBoard

	/***************************************
	 * MethodName: showTopArrayNumbers 
	 * parameters: beadArray: the number of beads for each player. 
	 * return: void
	 * description: Puts the array index numbers into the 0 - 5 bins
	 */
	public static void showTopArrayNumbers( int[ ] beadArray )
	{

		System.out.print ( "*      *" );
		for ( int index = 0; index < 6; index++ )
		{
			System.out.printf ( "%4d%3c", beadArray[index], '*' );
		}
		System.out.print ( "      *" );
		System.out.print ( "\n" );
	}// end of showTopArrayNumbers

	/*****************************
	 * MethodName: showBottomArrayNumbers 
	 * Return: Void 
	 * Parameters: beadArray the number of beads for each player
	 * Description: Puts the array index numbers into the 6 - 13 bins
	 */
	public static void showBottomArrayNumbers( int[ ] beadArray )
	{
		System.out.print ( "*" );
		for ( int index = 13; index > 5; index-- )
		{
			System.out.printf ( "%4d%3c", beadArray[index], '*' );
		}
		System.out.print ( "\n" );
	} // end of showBottomArrayNumbers

	/*******************************
	 * MethodName: showTopRowNumbers 
	 * Parameters: none 
	 * Return: void 
	 * Description: Put bin number on the six slots of the Mancala board
	 */
	public static void showTopRowNumbers( )
	{
		int lcv = 0;
		int number = 0; // top row bin number number
		while ( lcv < 6 )
		{
			System.out.printf ( "%4d%3c", number, '*' );
			lcv++;
			number++;
		}
		System.out.print ( "      *" );
		System.out.print ( "\n" );
	} // end of showTopRowNumbers

	/*******************************
	 * MethodName: showBottomRowNumbers 
	 * Parameters: None 
	 * Return: void 
	 * Description: Put bin number on bottom six slots of Mancala board
	 */
	public static void showBottomRowNumbers( )
	{
		int lcv = 12; // keeps track of loop count
		int number = 12; // bottom row bin number number
		while ( lcv != 6 )
		{
			System.out.printf ( "%4d%3c", number, '*' );
			lcv--;
			number--;
		}
		System.out.print ( "      *" );
		System.out.print ( "\n" );
	}// end of showBottomRowNumbers

	/*******************************
	 * MethodName: gameOverCheck 
	 * Parameters: beadArray: Number of beads for players and bins its associated with. 
	 * Return: int winner 
	 * Description: Checks if either top row or bottom row bin is 0 then returns a number based on who won.
	 */
	public static int gameOverCheck( int[ ] beadArray )
	{
		int player1Total = 0; // Total beads for player 1
		int player2Total = 0; // Total beads for player 2
		int player2EndBin = beadArray[13]; // Player 2 end bin
		int player1EndBin = beadArray[6]; // player 1 bin
		int winner = -1; // Number of the player who won
		for ( int index = 0; index < 6; index++ )
		{
			player1Total = player1Total + beadArray[index];
		}

		for ( int index = 12; index > 6; index-- )
		{
			player2Total = player2Total + beadArray[index];
		}

		if ( player1Total == 0 || player2Total == 0 )
		{

			for ( int index = 0; index < MAX; index++ )
			{
				beadArray[index] = 0;
			}

			beadArray[6] = player2Total + player1EndBin;
			beadArray[13] = player1Total + player2EndBin;
			if ( beadArray[6] == beadArray[13] )
			{
				winner = 0;
			}
			else if ( beadArray[6] > beadArray[13] )
			{
				winner = 1;
			}
			else
			{
				winner = 2;
			}

		}
		return winner;
	}// end of gameOverCheck method

	/*******************************
	 * MethodName: getStartingBin 
	 * Parameters: beadArray: Number of beads for players and bins its associated with. Player: which player is going Robot: is beep boop playing? hardMode: Is the robot going to be extra hard 
	 * Return: int bin 
	 * Description: Allows player to choose bin, and checks to see if its a valid choice based on which player is playing.
	 */
	public static int getStartingBin( int[ ] beadArray, int player, char robot, char hardMode )
	{
		int lowest; // lowest number in range
		int highest; // highest number in range
		int bin = 0; // player selected bin
		if ( player == 1 )
		{
			lowest = 0;
			highest = 5;
		}
		else
		{
			lowest = 7;
			highest = 12;
		}

		if ( robot == 'y' && player == 2 )
		{
			bin = goBeepBoop ( beadArray, hardMode );
			System.out.println ( "I, BeepBoop chose: " + bin + "!" );
		}
		else
		{
			do
			{
				System.out.println ( "Player " + player + " choose a bin between: " + lowest + " and " + highest );
				bin = input.nextInt ( );

			} while ( ( bin < lowest ) || ( bin > highest ) || ( beadArray[bin] == 0 ) );
		}
		return bin;
	}

	/*******************************
	 * MethodName: dropBeads 
	 * Parameters: beadArray: Number of beads for players and bins its associated with. player: which player is going robot: is beep boop playing? hardMode: is beepboop going to be a little harder? 
	 * Return: void
	 * Description: Player chooses which bin and it drops the beads from that into separate bins. Reruns until players turn is over.
	 */
	public static void dropBeads( int[ ] beadArray, int player, char robot, char hardMode )
	{
		int oppEndBin = 0; // Opponents End Bin
		int endBin = 0; // Current Players end bin
		int bin = 0; // which bin is picked
		int hand; // grabs and holds them beads, dawg.
		int gameOver; // checks whether or not game is over.

		if ( player == 1 )
		{
			endBin = 6;
			oppEndBin = 13;
		}
		else if ( player == 2 )
		{
			endBin = 13;
			oppEndBin = 6;
		}

		do
		{
			bin = getStartingBin ( beadArray, player, robot, hardMode );
			do
			{
				hand = beadArray[bin];
				beadArray[bin] = 0;

				do
				{
					bin++;
					if ( bin == oppEndBin )
					{
						bin++;
					}
					if ( bin >= MAX )
					{
						bin = 0;
					}

					beadArray[bin]++;
					hand--;
				} while ( hand > 0 );
			} while ( ( bin != endBin ) && ( beadArray[bin] > 1 ) ); // checks if bin is higher than one AND if bin is not
																						// players end bin!

			gameOver = gameOverCheck ( beadArray );
			drawBoard ( beadArray );
		} while ( ( bin == endBin ) && ( gameOver == -1 ) ); // checks if player ends in end bin AND makes sure game is
																				// not over yet!
	}

	/*******************************
	 * MethodName: goBeepBoop 
	 * Parameters: beadArray: Number of beads for players and bins its associated with. hardMode:Is beepboop being a bully? 
	 * Return: bin 
	 * Description: Robot chooses which bin to use, based on 3 things. Whether it ends in its own bin, if it will end in bin 0 of opponents bin, or if its the highest number on its side, has an optional hardmode.
	 */
	public static int goBeepBoop( int beadArray[], char hardMode )
	{
		int endBin = 13; // Beep Boops End bin
		int endBinCalc; // Calculation of beadArray[index] and endbin
		int binNumber; // the Bin number.
		int sabOpp = 0; // bin choice to Sabotage Opponents chance of winning
		int bin = 0; // which bin is chosen
		int highest = 0; // highest bead number that has appeared
		int perfect13 = -1; // Bin choice that makes it let BeepBoop choose again
		binNumber = 13; // BeepBoops endbin
		for ( int index = 12; index > 6; index-- )
		{
			binNumber--;
			endBinCalc = binNumber + beadArray[index];
			if ( endBinCalc == endBin )
			{
				perfect13 = index;
			}
			if ( endBin + 1 == endBinCalc )
			{
				sabOpp = index;
			}
			if ( highest < beadArray[index] )
			{
				highest = beadArray[index];
				bin = index;
			}
		}

		if ( perfect13 > 0 )
		{
			bin = perfect13;
		}
		if ( hardMode == 'y' )
		{
			if ( sabOpp > 0 )
			{
				bin = sabOpp;
			}
		}

		return bin;
	}//end of goBeepBoop

}
//problems: semicolon at the end of ifs. Figuring out how to stop the game if player ends in end bin and his bottom row is empty. As well as getting beepboop to be the monster that he is.