# MancalaGame
  - Version 1.0
  - Mancala Written in Java!
  
 # Features
  - Self Contained in Console, no GUI
  - Option for AI (with a hard mode!)
  
  # Questions?
  - Ask away! DM me. Or Follow me @TopTierPanda
